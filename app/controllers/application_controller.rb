class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_current_user

  def set_current_user
     # byebug
     ActiveRecord::Base.connection.execute("set session session.user_id to '#{current_user ? current_user.id : '0'}' ")
     ActiveRecord::Base.connection.execute("set session session.request_path to '#{request.fullpath}' ")
     # User.current = current_user
   end
end
