class Post < ApplicationRecord
  validates :title, :body, presence: true
  has_many :comments, dependent: :destroy
  belongs_to :user
  
  def to_param
    [id,title.parameterize].join("-")
  end
end
