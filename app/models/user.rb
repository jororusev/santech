class User < ApplicationRecord
  has_many :posts

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

def after_database_authentication
  # byebug
  ActiveRecord::Base.connection.execute("set session session.user_id to #{self.id} ")
  # ActiveRecord::Base.connection.execute("set session session.request_path to '' ")
end

# def self.current
#     Thread.current[:user]
# end
#
# def self.current=(user)
#     Thread.current[:user] = user
# end

end
