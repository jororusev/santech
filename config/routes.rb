Rails.application.routes.draw do
  namespace :admin do
    resources :users
    resources :comments
    resources :posts

    root to: "users#index"
  end

  devise_for :users
  resources :comments
  resources :posts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    root 'posts#index'
end
